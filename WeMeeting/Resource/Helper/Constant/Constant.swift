//
//  Constant.swift
//  SamplePopUp
//
//  Created by Hak Sreyneat on 6/10/22.
//

import Foundation

typealias Completion                = ()                -> Void
typealias Completion_Int            = (Int)             -> Void
typealias Completion_Bool           = (Bool)            -> Void
typealias Completion_NSError        = (NSError?)        -> Void
typealias Completion_String         = (String)          -> Void
typealias Completion_String_String  = (String, String)  -> Void

typealias Completion_String_Error   = (String, Error?)  -> Void
