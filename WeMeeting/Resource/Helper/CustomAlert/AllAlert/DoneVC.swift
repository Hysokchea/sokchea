//
//  DoneVC.swift
//  SamplePopUp
//
//  Created by Hak Sreyneat on 6/10/22.
//

import UIKit

class DoneVC: UIViewController {
    
    @IBOutlet weak var topView      : UIView!
    @IBOutlet weak var imageView    : UIImageView!
    @IBOutlet weak var titleLb      : UILabel!
    @IBOutlet weak var messgaeLbl   : UILabel!
    @IBOutlet weak var doneBtn      : UIButton!
    
    // Variable
    var imageString         = ""
    var titleString         = ""
    var massageString       = ""
    var confirmDoneBtn      = ""
    var topViewColor        = UIColor()
    var doneBtnColor        = UIColor()
    
    var doneAction : Completion_Bool = { _ in }
    var dismissAction : Completion_Bool = { _ in }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialized()
    }
    
    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @IBAction func didTapDoneBtnAction(_ sender: Any) {
        print("kkkk")
        
        self.dismiss(animated: true)
        doneAction(true)
        
    }
    
    private func initialized() {
        
        imageView?.image        = UIImage(named: imageString)
        titleLb?.text           = titleString
        messgaeLbl?.text        = massageString
        topView.backgroundColor = topViewColor
        doneBtn.backgroundColor = doneBtnColor
    }

}

extension UIViewController {
        
    func customDoneAlert(image: String, title: String, message: String, topColor: UIColor,done: UIColor, completion: @escaping Completion_Bool = { _ in }) {
        let vc = self.callCommonPopup(withStorybordName: "DoneSB", identifier: "DoneVC") as! DoneVC
        vc.imageString = image
        vc.titleString = title
        vc.massageString = message
        vc.topViewColor = topColor
        vc.doneBtnColor = done
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.doneAction = { _ in
            completion(true)
        }
        vc.dismissAction = { _ in
            completion(false)
        }
        self.present(vc, animated: true, completion: nil)
    
    }
    

}

