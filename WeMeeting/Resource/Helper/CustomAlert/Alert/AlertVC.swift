//
//  AlertVC.swift
//  SamplePopUp
//
//  Created by Huort Seanghay on 5/10/22.
//

import UIKit




class AlertVC: UIViewController {

    //MARK: - IBOutlet
    @IBOutlet weak var viewbackground: UIView!
    @IBOutlet weak var imgView          : UIImageView!
    @IBOutlet weak var titleLbl         : UILabel!
    @IBOutlet weak var messageLbl       : UILabel!
    
    @IBOutlet weak var noBtn            : UIButton!
    @IBOutlet weak var yesBtn           : UIButton!
    
    @IBOutlet weak var backgroundView   : UIView!
    @IBOutlet weak var topView          : UIView!
    
    //MARK: - Variable
    var imageString         = ""
    var titleString         = ""
    var messageString       = ""
    var confirmbtnString    = ""
    var topViewColor        = UIColor()
    
    var yesAction       : Completion_Bool = { _ in }
    var noAction        : Completion_Bool = { _ in }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialized()
        noBtn.backgroundColor           = .clear
        noBtn.layer.cornerRadius        = 15
        noBtn.layer.borderWidth         = 1
        noBtn.layer.borderColor         = UIColor.gray.cgColor

    }

    @IBAction func didTapNoBtnAction(_ sender: Any) {
        self.dismiss(animated: true)
        
    }
    @IBAction func didTapYesBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true)
        yesAction(true)

    }
    
    //MARK: - Private Func
    private func initialized() {
        imgView?.image              = UIImage(named: imageString)
        titleLbl?.text              = titleString
        messageLbl?.text            = messageString
        yesBtn?.setTitle(confirmbtnString, for: .normal)
        topView.backgroundColor     = topViewColor


    }
}

//MARK: - extension
extension UIViewController {

    //for pop up
    func callCommonPopup(withStorybordName storyboard: String, identifier: String) -> UIViewController {
            let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
            vc.modalPresentationStyle                       = .overFullScreen
            vc.modalTransitionStyle                         = .crossDissolve
            vc.providesPresentationContextTransitionStyle   = true
            vc.definesPresentationContext                   = true
            return vc
        }

    //call when we need to use
    func customAlert(image:String, title: String, message:String, yesTitle: String, topColor: UIColor, yesColor: UIColor, completion: @escaping Completion_Bool = {_ in}) {
        
        let vc = self.callCommonPopup(withStorybordName: "AlertSB", identifier: "AlertVC") as! AlertVC
        vc.imageString      = image
        vc.titleString      = title
        vc.messageString    = message
        vc.confirmbtnString = yesTitle
        vc.topViewColor     = topColor
        DispatchQueue.main.async {
            vc.yesBtn.backgroundColor = yesColor
        }
        
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.yesAction = { _ in
            completion(true)
        }
        vc.noAction = { _ in
            completion(false)
        }
        self.present(vc, animated: true, completion: nil)
    }
}
