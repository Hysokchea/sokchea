//
//  EmptyTableViewCell.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {
    
    let lineView    = UIView()
    let lable       = UILabel()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        lineView.removeFromSuperview()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(data:  MeetingModel.MeetingObject?) {
        if data?.isNow == true {
            lable.text = "Now"
            lable.textColor = .red
    
            lable.frame = CGRect(x: 20, y: data?.yPosition ?? 0, width: 80, height: 30)
            
            lineView.backgroundColor = .red
            lineView.frame = CGRect(x: 20, y: data?.yPosition ?? 0, width: Int(self.frame.width) - 20, height: 2)
            self.contentView.addSubview(lable)
            self.contentView.addSubview(lineView)
        }
        
    }

}
