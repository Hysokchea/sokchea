//
//  HistoryTableViewCell.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
