//
//  MeetingTableViewCell.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import UIKit

class MeetingTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblOption: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    let lineView         = UIView()
    let lable            = UILabel()
    let backgroundLetter = UIView()
    var owner            : UIViewController!
    
    let randomColor : [UIColor] = [.red, .blue, .green, .orange, .yellow, .purple, .tertiaryLabel, .gray, .magenta, .link, .systemYellow, .systemRed]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(data:  MeetingModel.MeetingObject?) {
        
        if data?.isNow == true {
            lable.text = "Now"
            lable.textColor                  = .white
            lable.textAlignment              = .center
            lable.frame = CGRect(x: 10, y: (data?.yPosition ?? 0) - 10, width: 50, height: 20)
            
            lable.backgroundColor            = .red
            backgroundLetter.cornerRadius    = 15
            backgroundLetter.backgroundColor = .red
            backgroundLetter.frame           = CGRect(x: 10, y: (data?.yPosition ?? 0) - 10, width: 50, height: 20)
            backgroundLetter.cornerRadius    = 15
            lineView.backgroundColor         = .red
            lineView.frame = CGRect(x: 60, y: (data?.yPosition ?? 0), width: Int(self.frame.width) - 20, height: 2)
            
            self.contentView.addSubview(backgroundLetter)
            self.contentView.addSubview(lable)
            self.contentView.addSubview(lineView)
        }
        self.viewColor.backgroundColor  = randomColor.randomElement()
        self.lblTitle.text              = data?.meetingName
        self.lblDescription.text        = data?.meetingDesc
        self.lblTime.text               = data?.meetingTime
        self.imgProfile.image           = UIImage(named: data?.profileName ?? "person1")
    }
}
