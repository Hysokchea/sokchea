//
//  AlertTableViewCell.swift
//  WeMeeting
//
//  Created by Lyheang Ky on 14/10/22.
//

import UIKit

class AlertTableViewCell: UITableViewCell {

    @IBOutlet weak var imageAlert : UIImageView!
    @IBOutlet weak var lableTitle : UILabel!
    @IBOutlet weak var lableDesc  : UILabel!
    @IBOutlet weak var lableTime  : UILabel!
    @IBOutlet weak var alertView  : UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setValue(data : MeetingModel.MeetingRoom){
        imageAlert?.image            = UIImage(named: "warning")
        alertView.layer.cornerRadius = 15
        alertView.backgroundColor    = .yellow
        lableDesc?.text              = data.meetingName
        lableTime?.text              = data.date
        lableTime.textColor          = .red
        lableTitle?.text             = data.title
    }

}
