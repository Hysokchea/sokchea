//
//  HomeVC.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    
    
    private let imageView = UIImageView(image: UIImage(named: "panda"))
    private var shouldResize: Bool?
    
    var meetingVM = MeetingVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        shouldResize = true
        self.setUpNavigation()
    
        //Fetch data from VM
        self.meetingVM.fetchMeetingData()
        self.meetingVM.fetchIsNow()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        showImage(false)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showImage(true)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let shouldResize = shouldResize
            else {
            assertionFailure("shouldResize wasn't set. reason could be non-handled device orientation state");
            return
        }

        if shouldResize {
            moveAndResizeImageForPortrait()
        }
    }
    
    private func setUpNavigation() {
        // Set NavigationBar title
        self.title = "Home"
        
        // Remove NavigationBar's border
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.navigationController?.navigationBar.shadowImage        = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
         
        // Set NavigationBar for both title and large title
    
        //self.navigationController?.navigationBar.titleTextAttributes        = [NSAttributedString.Key.font: UIFont(name: "Rubik-Bold", size: 20) ]
        //self.navigationController?.navigationBar.largeTitleTextAttributes   = [NSAttributedString.Key.font: UIFont(name: "Rubik-Bold", size: 28)]
         
        // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        navigationBar.addSubview(imageView)
        
        imageView.layer.cornerRadius    = Const.ImageSizeForLargeState / 2
        imageView.clipsToBounds         = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -Const.ImageRightMargin),
            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
        ])
    }
 
}
//MARK: - UITableView
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.meetingVM.meetingData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType = self.meetingVM.meetingData[section].sectionType
        
        switch sectionType {
        case .AlertSection:
            return self.meetingVM.meetingData[section].meetingRoom.count
        case .CalendarSection, .HistorySection:
            return 0
        case .HeaderSection:
            return self.meetingVM.meetingData[section].meetingObject.count
            
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionType = self.meetingVM.meetingData[section].sectionType
        
        switch sectionType {
            case .AlertSection:
                return 0
            default:
                return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionType = self.meetingVM.meetingData[section].sectionType
        
        switch sectionType {
            case .CalendarSection:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarTableViewCell") as! CalendarTableViewCell
                return cell
                
            case .HistorySection:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell") as! HistoryTableViewCell
                return cell
            case .HeaderSection:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
                cell.lblTitle.text  = self.meetingVM.meetingData[section].sectionTitle
                return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowType = self.meetingVM.meetingData[indexPath.section].meetingObject[indexPath.row].isEmptyRow
         
        switch rowType {
        case .Yes:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as! EmptyTableViewCell
            cell.configCell(data: self.meetingVM.meetingData[indexPath.section].meetingObject[indexPath.row])
            return cell
        case .No:
            // Case NotEmptyRow
            let cell      = tableView.dequeueReusableCell(withIdentifier: "MeetingTableViewCell", for: indexPath) as! MeetingTableViewCell
            cell.configCell(data: self.meetingVM.meetingData[indexPath.section].meetingObject[indexPath.row])
            return cell
        case .Alert:
            // Cell.setValue
            print("Alert Cell")
            let cellAlert = tableView.dequeueReusableCell(withIdentifier: "AlertTableViewCell", for: indexPath) as! AlertTableViewCell
            cellAlert.setValue(data: self.meetingVM.meetingData[indexPath.section].meetingRoom[indexPath.row])
            return cellAlert
        }
    }
    
}

//MARK: - Custom UINavigationBar
extension HomeVC {
    
    /// WARNING: Change these constants according to your project's design
    private struct Const {
        
        /// Image height/width for Small NavBar state
        static let ImageSizeForSmallState: CGFloat = 30
        
        /// Image height/width for Large NavBar state
        static let ImageSizeForLargeState: CGFloat = 50
        
        /// Height of NavBar for Small state. Usually it's just 44
        static let NavBarHeightSmallState: CGFloat = 44
        
        /// Height of NavBar for Large state. Usually it's just 96.5 but if you have a custom font for the title, please make sure to edit this value since it changes the height for Large state of NavBar
        static let NavBarHeightLargeState: CGFloat = 96.5
        
        /// Margin from right anchor of safe area to right anchor of Image
        static let ImageRightMargin: CGFloat = 20
        
        /// Margin from bottom anchor of NavBar to bottom anchor of Image for Large NavBar state
        static let ImageBottomMarginForLargeState: CGFloat = 0
        
        /// Margin from bottom anchor of NavBar to bottom anchor of Image for Small NavBar state
        static let ImageBottomMarginForSmallState: CGFloat = 16
          
        /// Image height/width for Landscape state
        static let ScaleForImageSizeForLandscape: CGFloat = 0.65
    }
     
    /// Show or hide the image from NavBar while going to next screen or back to initial screen
    /// - Parameter show: show or hide the image from NavBar
    private func showImage(_ show: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.imageView.alpha = show ? 1.0 : 0.0
        }
    }
    
    private func resizeImageForLandscape() {
        let yTranslation = Const.ImageSizeForLargeState * Const.ScaleForImageSizeForLandscape
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: Const.ScaleForImageSizeForLandscape, y: Const.ScaleForImageSizeForLandscape)
            .translatedBy(x: 0, y: yTranslation)
    }
    
    private func moveAndResizeImageForPortrait() {
        guard let height = navigationController?.navigationBar.frame.height else { return }

        let coeff: CGFloat = {
            let delta = height - Const.NavBarHeightSmallState
            let heightDifferenceBetweenStates = (Const.NavBarHeightLargeState - Const.NavBarHeightSmallState)
            return delta / heightDifferenceBetweenStates
        }()

        let factor = Const.ImageSizeForSmallState / Const.ImageSizeForLargeState

        let scale: CGFloat = {
            let sizeAddendumFactor = coeff * (1.0 - factor)
            return min(1.0, sizeAddendumFactor + factor)
        }()

        // Value of difference between icons for large and small states
        let sizeDiff = Const.ImageSizeForLargeState * (1.0 - factor) // 8.0
        
        let yTranslation: CGFloat = {
            /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = Const.ImageBottomMarginForLargeState - Const.ImageBottomMarginForSmallState + sizeDiff
            return max(0, min(maxYTranslation, (maxYTranslation - coeff * (Const.ImageBottomMarginForSmallState + sizeDiff))))
        }()
            
        let xTranslation = max(0, sizeDiff - coeff * sizeDiff)
        
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: scale, y: scale)
            .translatedBy(x: xTranslation, y: yTranslation)
    }
    
}

