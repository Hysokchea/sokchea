//
//  MeetingModel.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import Foundation
enum SectionTypeMore {
    case CalendarSection
    case HistorySection
    case HeaderSection
    case AlertSection
}

enum IsEmptyRow {
    case Yes
    case Alert
    case No
}



struct MeetingModel {
    let sectionType     : SectionTypeMore?
    let sectionTitle    : String?
    var meetingObject   : [MeetingObject]
    let meetingRoom     : [MeetingRoom]
    struct MeetingObject {
        let isEmptyRow  : IsEmptyRow
        let meetingName : String
        let profileName : String
        let meetingDesc : String
        var isNow       : Bool
        let meetingTime : String
        var yPosition   : Int
    }
    struct MeetingRoom{
        let title       : String
        let meetingName : String
        let date        : String
    }
}





