//
//  MeetingVM.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import Foundation
class MeetingVM {
    
    var meetingData    = [MeetingModel]()
    // Init Data
    func fetchMeetingData() {
        
        self.meetingData = [
            MeetingModel(sectionType: .AlertSection, sectionTitle: "",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Alert, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "Your Meeting start soon", meetingName: "Angkor Meeting Room", date: "3:15")]),
            
            MeetingModel(sectionType: .CalendarSection, sectionTitle: "",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            
            //MeetingModel(sectionType: .HistorySection, sectionTitle: "",
            //             meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "08:00 AM",
                         meetingObject: [
                            MeetingModel.MeetingObject(isEmptyRow: .No, meetingName: "Mekong Room", profileName: "person2", meetingDesc: "Team Meeting", isNow: false, meetingTime: "8:30AM - 9:00AM",yPosition: 0)
                         ], meetingRoom: [MeetingModel.MeetingRoom(title: "Your Meeting start soon!", meetingName: "Angkor Room", date: "4:15")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "09:00 AM",
                         meetingObject: [
                            MeetingModel.MeetingObject(isEmptyRow: .No, meetingName: "Angkor Room", profileName: "person4", meetingDesc: "Team Meeting", isNow: false, meetingTime: "9:30AM - 10:00AM",yPosition: 0)
                         ], meetingRoom: [MeetingModel.MeetingRoom(title: "Your Meeting start soon!", meetingName: "Mekong Room", date: "5:00")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "10:00 AM",
                         meetingObject: [
                            MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "11:00 AM",
                         meetingObject: [
                            MeetingModel.MeetingObject(isEmptyRow: .No, meetingName: "Mekong Room", profileName: "person1", meetingDesc: "Sharing Tech-Talk", isNow: false, meetingTime: "9:00AM - 9:30AM",yPosition: 0),
                         ], meetingRoom: [MeetingModel.MeetingRoom(title: "Your Meeting start soon!", meetingName: "Mekong room", date: "3:30")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "12:00 PM",
                         meetingObject: [
                            MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "01:00 AM",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .No, meetingName: "Mekong Room", profileName: "person3", meetingDesc: "Weekly Meeting", isNow: false, meetingTime: "1:00PM - 2:00PM",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "Your Meeting start soon!", meetingName: "Mekong Room", date: "2:30")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "02:00 PM",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "03:00 PM",
                         meetingObject: [
                            MeetingModel.MeetingObject(isEmptyRow: .No, meetingName: "Mekong Room", profileName: "person4", meetingDesc: "Sharing Tech-Talk", isNow: false, meetingTime: "3:00PM - 3:30PM",yPosition: 0),
                            MeetingModel.MeetingObject(isEmptyRow: .No, meetingName: "Angkor Room", profileName: "person1", meetingDesc: "Team Meeting", isNow: false, meetingTime: "3:30PM - 4:00PM",yPosition: 0)
                         ], meetingRoom: [MeetingModel.MeetingRoom(title: "Your Meeting start soon!", meetingName: "Mekong Room", date: "3:30")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "04:00 PM",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "4:30PM - 5:00PM",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "05:00 PM",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "5:30PM - 6:00PM",yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "06:00 PM",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "6:30PM - 7:00PM", yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "07:00 PM",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "", yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "08:00 PM",
                         meetingObject: [MeetingModel.MeetingObject(isEmptyRow: .Yes, meetingName: "", profileName: "", meetingDesc: "", isNow: false, meetingTime: "", yPosition: 0)], meetingRoom: [MeetingModel.MeetingRoom(title: "", meetingName: "", date: "")]),
            MeetingModel(sectionType: .HeaderSection, sectionTitle: "09:00 PM",
                         meetingObject: [
                            MeetingModel.MeetingObject(isEmptyRow: .No, meetingName: "Mekong Room", profileName: "person4", meetingDesc: "Sharing Tech-Talk", isNow: false, meetingTime: "9:30PM - 10:00PM",yPosition: 0),
                         ], meetingRoom: [MeetingModel.MeetingRoom(title: "Your Meeting start soon!", meetingName: "Mekong Room", date: "3:30")]),
        ]
        
        
    }
    func fetchIsNow() {
        let date                    = Date()
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "hh:mm a"

        let twentyFourHour  = dateFormatter.string(from: date)
        let hour            = twentyFourHour.components(separatedBy: ":").first ?? ""
        let minute          = twentyFourHour.components(separatedBy: ":").last?.components(separatedBy: " ").first ?? ""
        let ampm            = twentyFourHour.components(separatedBy: ":").last?.components(separatedBy: " ").last ?? ""

        let section = self.meetingData.firstIndex(where: { $0.sectionTitle == "\(hour):00 \(ampm)" }) ?? 0

        // 60 = 60 minutes
        let minutesInEachRow = 60 / self.meetingData[section].meetingObject.count

        for index in 0..<self.meetingData[section].meetingObject.count {
            let range       = (index * minutesInEachRow) +  minutesInEachRow
            let isNow       = Int(minute) ?? 0 < range ? true : false
            self.meetingData[section].meetingObject[index].isNow = isNow
            let fixedheight = self.meetingData[section].meetingObject[index].isEmptyRow == .Yes ? 30 : 120
            self.meetingData[section].meetingObject[index].yPosition = (Int(minute) ?? 0) * (fixedheight / 60)

            if isNow {
                break
            }
        }
    }
}
