//
//  ButtomStackItem.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import Foundation
struct ButtomStackItem{
    var title       : String
    var image       : String
    var isSelected  : Bool
    init(title: String,
         image: String,
         isSelected: Bool = false) {
        self.title      = title
        self.image      = image
        self.isSelected = isSelected
    }
    mutating func setTitle(title : String){
        self.title = title
    }
    mutating func setImage(image : String){
        self.image = image
    }
    mutating func setIsSelected(isSelected : Bool){
        self.isSelected = isSelected
    }
}

