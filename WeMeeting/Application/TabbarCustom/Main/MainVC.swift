//
//  MainVC.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import UIKit

class MainVC: UIViewController {
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var trailingTabContraint: NSLayoutConstraint!
    @IBOutlet weak var leadingTabContraint: NSLayoutConstraint!
    @IBOutlet weak var createMeetingView  : UIView!
    @IBOutlet weak var homeView           : UIView!
    @IBOutlet weak var notificationView   : UIView!
    
    @IBOutlet weak var imageTabbar        : UIImageView!
    
    @IBOutlet weak var tabbarbg           : UIView!    
    
    var currentIndex = 0
    
    lazy var tabs: [StackViewItem] = {
        var items = [StackViewItem]()
        for _ in 0..<3 {
            items.append(StackViewItem.newInstance)
        }
        return items
    }()
    
    lazy var tabModels: [ButtomStackItem] = {
        return [
            ButtomStackItem(title: "Home", image: "homeHighlight"),
            ButtomStackItem(title: "Notification", image: "notification_white")
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTabs()
        self.setUpImage()
        self.homeView?.isHidden          = false
        self.createMeetingView?.isHidden = true
        self.notificationView?.isHidden  = true
        trailingTabContraint.constant  = 60
        leadingTabContraint.constant   = 50
    }
    func setUpImage(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            imageTabbar.isUserInteractionEnabled = true
            imageTabbar.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let mainStoryboard   = UIStoryboard(name: "CreateNewMeetingSB", bundle: nil)
            let goController = mainStoryboard.instantiateViewController(withIdentifier: "CreateNewMeetingVC") as! CreateNewMeetingVC
            self.present(goController, animated: true)
    }
    
    func setupTabs() {
        for (index, model) in self.tabModels.enumerated() {
            let tabView         = self.tabs[index]
            var newModel        = model
            newModel.setIsSelected(isSelected: index == 0) 
            //model.isSelected    = index == 0
            tabView.item        = newModel
            tabView.delegate    = self
            self.stackView.addArrangedSubview(tabView)
        }
    }
    func setConstraint() {
        leadingTabContraint.constant = 50
        
    }
}
extension MainVC: StackItemViewDelegate {
    
    func handleTap(_ view: StackViewItem) {
        self.tabs[self.currentIndex].isSelected = false
        view.isSelected                         = true
        self.currentIndex                       = self.tabs.firstIndex(where: { $0 === view }) ?? 0
        // MARK: - heartView => ContainView, 0 = index
        if currentIndex != 0{
            self.homeView.isHidden          = true
            self.notificationView.isHidden  = false
            leadingTabContraint.constant    = 60
            trailingTabContraint.constant   = 30
        }
        else if currentIndex != 1{
            self.homeView.isHidden         = false
            self.notificationView.isHidden = true
            trailingTabContraint.constant  = 60
            leadingTabContraint.constant   = 50
        }
    }
}
