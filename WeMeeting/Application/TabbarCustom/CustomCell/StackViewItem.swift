//
//  StackViewItem.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import UIKit

protocol StackItemViewDelegate: AnyObject {
    func handleTap(_ view: StackViewItem)
}

class StackViewItem: UIView {
    
    @IBOutlet weak var highlightView    : UIView!
    @IBOutlet weak var titleLabel       : UILabel!
    @IBOutlet weak var imgView          : UIImageView!
    
    
    private let higlightBGColor = hexStringToUIColor(hex: "#EDF5FF")
    
    
    static var newInstance: StackViewItem {
        return Bundle.main.loadNibNamed(
            "StackViewItem",
            owner: nil,
            options: nil
        )?.first as! StackViewItem
    }
    
    weak var delegate: StackItemViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        highlightView.layer.cornerRadius = 15
        self.addTapGesture()
    }
    
     var isSelected: Bool = false{
        willSet {
            self.updateUI(isSelected: newValue)
        }
    }
    
    var item: Any? {
        didSet {
            self.configure(self.item)
        }
    }
    
    private func configure(_ item: Any?) {
        guard let model = item as? ButtomStackItem else { return }
        self.titleLabel.text    = model.title
        self.imgView.image      = UIImage(named: model.image)
        self.isSelected         = model.isSelected
    }
    
    private func updateUI(isSelected: Bool) {
        guard var model = item as? ButtomStackItem else { return }
        model.isSelected = isSelected
        let options: UIView.AnimationOptions = isSelected ? [.curveEaseIn] : [.curveEaseOut]
        
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.5, options: options, animations: {
            self.titleLabel.text = isSelected ? model.title : ""
            let color = isSelected ? self.higlightBGColor : .white
            self.highlightView.backgroundColor = color
            (self.superview as? UIStackView)?.layoutIfNeeded()
        }, completion: nil)
    }
    
    // function for on Tap
    func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleGesture(_ :)))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleGesture(_ sender: UITapGestureRecognizer){
        self.delegate?.handleTap(self)
    }
}
// MARK: HexString Color
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
        return UIColor.gray
    }

    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

