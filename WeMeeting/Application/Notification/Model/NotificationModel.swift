//
//  NotificationModel.swift
//  WeMeet
//
//  Created by Lyheang Ky on 8/10/22.
//

import Foundation


enum NotiRowType {
    case NotificationHeader
    case NotificationList
}

struct NotifacationModel<T>{
    var value: T?
}

struct HeaderInfo {
    var all     : String
    var unread  : String
    var readAll : String
    var rowType : NotiRowType
}

struct ContentInfo {
    var image: String?
    var title: String?
    var desc: String?
    var rowType: NotiRowType
}
