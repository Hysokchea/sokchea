//
//  NotificationVC.swift
//  WeMeet
//
//  Created by Lyheang Ky on 9/10/22.
//

import UIKit


typealias interfaceCollectionView = UICollectionViewDelegate & UICollectionViewDataSource
typealias interfaceTableView      = UITableViewDelegate & UITableViewDataSource

class NotificationVC: UIViewController{

    @IBOutlet weak var tableView: UITableView!
    
    private let imageView = UIImageView(image: UIImage(named: "panda"))
    
    var notificationVM = NotificationVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //======================================
        self.notificationVM.insertData()
        //============================
        self.setUpNavigation()
    }
    private func setUpNavigation() {
        // Set NavigationBar title
        self.title = "Notification"
        
        // Remove NavigationBar's border
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.navigationController?.navigationBar.shadowImage        = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
         
        // Set NavigationBar for both title and large title
        //self.navigationController?.navigationBar.titleTextAttributes        = [NSAttributedString.Key.font: UIFont(name: "Rubik-Bold", size: 20)!]
        //self.navigationController?.navigationBar.largeTitleTextAttributes   = [NSAttributedString.Key.font: UIFont(name: "Rubik-Bold", size: 28)!]
    
        // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        navigationBar.addSubview(imageView)
        
        imageView.layer.cornerRadius    = Const.ImageSizeForLargeState / 2
        imageView.clipsToBounds         = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -Const.ImageRightMargin),
            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
        ])
    }

}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource{
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//
//        return notificationVM.model.count
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let sectionType = notificationVM.model[ind]
//        let sectionType = self.notificationVM.model[section].type
//        switch sectionType {
//
//        }
        
//        if notificationVM.model.count != 0{
//            return notificationVM.model.count
//        }else{
//            return 2
//        }
        
        return notificationVM.model.count
    
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            let data = notificationVM.model[indexPath.row].value as! HeaderInfo
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlertTVC", for: indexPath) as! AlertTVC
            cell.setNotification(data: data)
            return cell
        } else {
            if notificationVM.model.count != 0 {
                let data = notificationVM.model[indexPath.row].value as! ContentInfo
                let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTVC", for: indexPath) as! NotificationTVC
                cell.configData(data: data)
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoNotificationCell", for: indexPath) as! NoNotificationCell
                return cell
            }
        }
        
        
//        let data = notificationVM.model[i
//        switch data.rowType{
//        case .NotificationHeader:
//            
//        case .NotificationContent:
//            
//            if notificationVM.model.count != 0 {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTVC", for: indexPath) as! NotificationTVC
//                cell.configData(data: data.value as! ContentCell)
//                return cell
//            }else{
//                let cell = tableView.dequeueReusableCell(withIdentifier: "NoNotificationCell", for: indexPath) as! NoNotificationCell
//                return cell
//            }
//        }
    }
    
}

extension NotificationVC {

    /// WARNING: Change these constants according to your project's design
    private struct Const {

        /// Image height/width for Small NavBar state
        static let ImageSizeForSmallState: CGFloat = 30

        /// Image height/width for Large NavBar state
        static let ImageSizeForLargeState: CGFloat = 50

        /// Height of NavBar for Small state. Usually it's just 44
        static let NavBarHeightSmallState: CGFloat = 44

        /// Height of NavBar for Large state. Usually it's just 96.5 but if you have a custom font for the title, please make sure to edit this value since it changes the height for Large state of NavBar
        static let NavBarHeightLargeState: CGFloat = 96.5

        /// Margin from right anchor of safe area to right anchor of Image
        static let ImageRightMargin: CGFloat = 20

        /// Margin from bottom anchor of NavBar to bottom anchor of Image for Large NavBar state
        static let ImageBottomMarginForLargeState: CGFloat = 0

        /// Margin from bottom anchor of NavBar to bottom anchor of Image for Small NavBar state
        static let ImageBottomMarginForSmallState: CGFloat = 16

        /// Image height/width for Landscape state
        static let ScaleForImageSizeForLandscape: CGFloat = 0.65
    }

    /// Show or hide the image from NavBar while going to next screen or back to initial screen
    ///
    /// - Parameter show: show or hide the image from NavBar
    private func showImage(_ show: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.imageView.alpha = show ? 1.0 : 0.0
        }
    }

    private func resizeImageForLandscape() {
        let yTranslation = Const.ImageSizeForLargeState * Const.ScaleForImageSizeForLandscape
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: Const.ScaleForImageSizeForLandscape, y: Const.ScaleForImageSizeForLandscape)
            .translatedBy(x: 0, y: yTranslation)
    }

    private func moveAndResizeImageForPortrait() {
        guard let height = navigationController?.navigationBar.frame.height else { return }

        let coeff: CGFloat = {
            let delta = height - Const.NavBarHeightSmallState
            let heightDifferenceBetweenStates = (Const.NavBarHeightLargeState - Const.NavBarHeightSmallState)
            return delta / heightDifferenceBetweenStates
        }()

        let factor = Const.ImageSizeForSmallState / Const.ImageSizeForLargeState

        let scale: CGFloat = {
            let sizeAddendumFactor = coeff * (1.0 - factor)
            return min(1.0, sizeAddendumFactor + factor)
        }()

        // Value of difference between icons for large and small states
        let sizeDiff = Const.ImageSizeForLargeState * (1.0 - factor) // 8.0

        let yTranslation: CGFloat = {
            /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = Const.ImageBottomMarginForLargeState - Const.ImageBottomMarginForSmallState + sizeDiff
            return max(0, min(maxYTranslation, (maxYTranslation - coeff * (Const.ImageBottomMarginForSmallState + sizeDiff))))
        }()

        let xTranslation = max(0, sizeDiff - coeff * sizeDiff)

        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: scale, y: scale)
            .translatedBy(x: xTranslation, y: yTranslation)
    }

}
