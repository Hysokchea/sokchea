//
//  AlertTVC.swift
//  WeMeeting
//
//  Created by Lyheang Ky on 15/10/22.
//

import UIKit

class AlertTVC: UITableViewCell {
    @IBOutlet weak var btbNotification: UIButton!
    
    func setNotification(data: HeaderInfo){
        btbNotification.setTitle(data.all, for: .normal)
    }
}
