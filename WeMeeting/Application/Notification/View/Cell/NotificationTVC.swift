//
//  NotificationTVC.swift
//  WeMeeting
//
//  Created by Lyheang Ky on 15/10/22.
//

import UIKit

class NotificationTVC: UITableViewCell {

    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var lableDesc: UILabel!
    
    
    func configData(data : ContentInfo){
        imageProfile.image  = (UIImage(named: data.image ?? ""))
        imageProfile.makeRounded()
        labelTitle?.text    = data.title
        lableDesc?.text     = data.desc
    }
}
extension UIImageView{
    func makeRounded() {
        let radius = self.frame.width/2.0
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}
