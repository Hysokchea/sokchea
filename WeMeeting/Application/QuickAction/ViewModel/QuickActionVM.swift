//
//  QuickActionVM.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 10/10/22.
//

import Foundation
import UIKit

class QuickActionVM{
    
    var dataQuickAction       = [DataInForQuick<Any>]()
    var titleQuick          = [TitleQuick]()
    var quickAction        = [QuickAction]()
   
    
    func getQuickActionData(){
        
        
        titleQuick = [
            TitleQuick(
                title       : "title_quickaction".localized,
                subTitle    : "sub_title_quickaction".localized,
                type        : .TITLE_QIUCK
            )
        ]
        
        quickAction = [
            QuickAction(
                iconQuick       : "icon-copy",
                textQuick      : "copy_invitation".localized,
                type        : .Quick_ACTION
            ),
            QuickAction(
                iconQuick       : "icon-share",
                textQuick      : "share_media".localized,
                type        : .Quick_ACTION
            ),
            QuickAction(
                iconQuick       : "icon-view",
                textQuick      : "view_creator".localized,
                type        : .Quick_ACTION
            ),
            QuickAction(
                iconQuick       : "icon-duplicate",
                textQuick      : "duplicate".localized,
                type        : .Quick_ACTION
            ),
            QuickAction(
                iconQuick       : "icon-edit",
                textQuick      : "edit_info".localized,
                type        : .Quick_ACTION
            ),
            QuickAction(
                iconQuick       : "icon-delete",
                textQuick      : "remove".localized,
                
                type        : .Quick_ACTION
            )
        ]
        

        dataQuickAction = [
        
            DataInForQuick(type: .TITLE_QIUCK,            value: titleQuick),
            DataInForQuick(type: .Quick_ACTION,              value: quickAction),
            
        ]
    }
}
