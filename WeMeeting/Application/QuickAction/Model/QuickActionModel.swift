//
//  QuickActionModel.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 10/10/22.
//

import Foundation



    
    enum TypeCellQuick: String{

        case TITLE_QIUCK
        case Quick_ACTION

    }
enum CheckColor: String{
    case Default_Blue = "rejectType"
    case Custome_Red  = "acceptType"
}

    struct DataInForQuick<T>{
        var type  : TypeCellQuick
        var value : T?
    }

    struct TitleQuick{
        var title       : String?
        var subTitle    : String?
        var type        : TypeCellQuick?
    }

    struct QuickAction{
        var iconQuick       : String?
        var textQuick       : String?
        var color           : CheckColor?
        var type            : TypeCellQuick?
    }

