//
//  ButtonActionVC.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 10/10/22.
//

import UIKit

@available(iOS 16.0, *)
class ButtonActionVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func ActionBtn(_ sender: Any) {
        print("Go to login screen")
        

        let storyboard = UIStoryboard(name: "QuickActionSB", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuickActionVC")
        
        if #available(iOS 15.0, *) {
            if let sheet = vc.sheetPresentationController {
                let smallId = UISheetPresentationController.Detent.Identifier("small")
                let smallDetent = UISheetPresentationController.Detent.custom(identifier: smallId) { context in
                    return 400
                }
                
                sheet.detents = [smallDetent, .large()]
                    sheet.largestUndimmedDetentIdentifier = .medium

                }
                present(vc, animated: true, completion: nil)
        }
    }

}
