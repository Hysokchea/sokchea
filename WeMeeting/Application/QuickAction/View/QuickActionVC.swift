//
//  QuickActionVC.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 10/10/22.
//

import UIKit

class QuickActionVC: UIViewController {

    var quickActionVM = QuickActionVM()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.quickActionVM.getQuickActionData()
    }
    

}

extension QuickActionVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionQuick = quickActionVM.dataQuickAction[section].type
        switch sectionQuick {
            
            
        case .TITLE_QIUCK:
            return quickActionVM.titleQuick.count
            
        case .Quick_ACTION:
            return quickActionVM.quickAction.count
            
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return quickActionVM.dataQuickAction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionQuick = quickActionVM.dataQuickAction[indexPath.section].type
        
        switch sectionQuick {
       
            
        case .TITLE_QIUCK:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleQuickCell", for: indexPath) as! TitleQuickCell
            cell.titleQuick.text  =  quickActionVM.titleQuick[indexPath.row].title
            cell.subtitleQuick.text     =  quickActionVM.titleQuick[indexPath.row].subTitle
            return cell
            
        case .Quick_ACTION:
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuickActionCell", for: indexPath) as! QuickActionCell
            
            cell.textActionQuickLbl.text = quickActionVM.quickAction[indexPath.row].textQuick ?? ""
            cell.iconQuick.image = UIImage(named: quickActionVM.quickAction[indexPath.row].iconQuick ?? "")
            
            let type = quickActionVM.quickAction[indexPath.row].color
            
            if type == .Custome_Red{
               
                cell.textActionQuickLbl.textColor = UIColor(hexString: "#FF0000")
            }else{
                cell.textActionQuickLbl.textColor = UIColor(hexString: "#1075AC")
               
            }
            return cell
            
       
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let selectionCell: UITableViewCell = tableView.cellForRow(at: indexPath)!

                selectionCell.contentView.backgroundColor = UIColor(hexString: "#EDF5FF")
        }
        
        func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
            let selectionCell: UITableViewCell = tableView.cellForRow(at: indexPath)!

                selectionCell.contentView.backgroundColor = UIColor(named: "#0075E3") //Clear color
        }
    
}

