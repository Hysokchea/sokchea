//
//  QuickActionCell.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 10/10/22.
//

import UIKit

class QuickActionCell: UITableViewCell {

    @IBOutlet weak var iconQuick: UIImageView!
    @IBOutlet weak var textActionQuickLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
