//
//  TitleQuickCell.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 10/10/22.
//

import UIKit

class TitleQuickCell: UITableViewCell {

    @IBOutlet weak var subtitleQuick: UILabel!
    @IBOutlet weak var titleQuick: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
