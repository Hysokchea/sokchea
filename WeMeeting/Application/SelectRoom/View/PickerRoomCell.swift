//
//  PickerRoomCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 14/10/22.
//

import UIKit

class PickerSelectRoomCell: UITableViewCell {

    
    @IBOutlet weak var pickerView: UIPickerView!
    
    var selectRoomVM = SelectRoomVM()
    
    var datapicker = ["Angkor room", "jeju"]
    override func awakeFromNib() {
        super.awakeFromNib()
        pickerView.dataSource = self
        pickerView.delegate = self
        selectRoomVM.initCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension PickerSelectRoomCell: UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let data = self.selectRoomVM.cells[1]
        return (data.value as! PickerRoomCell).valuePicker.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let data = self.selectRoomVM.cells[1]
        let cellPicker = (data.value as! PickerRoomCell).valuePicker[row]
        return cellPicker
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
}

