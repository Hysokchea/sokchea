//
//  SelectRoomVC.swift
//  WeMeetingSample
//
//  Created by sokchea on 14/10/22.
//

import UIKit

class SelectRoomVC: UIViewController, UISheetPresentationControllerDelegate {

    var selectRoomVM = SelectRoomVM()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        tableView.delegate = self
        tableView.dataSource = self
        selectRoomVM.initCell()
    }
}

extension SelectRoomVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectRoomVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.selectRoomVM.cells[indexPath.row]
        
        switch data.rowType {
            
        case.contentCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectRoomSheetContentCell", for: indexPath) as! SelectRoomSheetContentCell
            cell.configData(data: data.value as! ContentCell)
     
            return cell
            
        case .pickerRoomCell:
            var cell = tableView.dequeueReusableCell(withIdentifier: "PickerSelectRoomCell", for: indexPath) as! PickerSelectRoomCell
//            let pickerCell = data.value as! PickerRoomCell
//            cell.pickerView.tag = indexPath.row
            return cell
           
        case .buttonCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonTableCell", for: indexPath) as! ButtonTableCell
            let valuePick = data.value as! ButtonCell
            cell.doneBtn.setTitle(valuePick.titleBtn, for: .normal)
            cell.controller = self
            return cell
        default:
           return UITableViewCell()
          
        }
    }
}

