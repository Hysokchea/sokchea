//
//  SelectRoomSheetContentCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 14/10/22.
//

import UIKit

class SelectRoomSheetContentCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
  
    }
    
    func configData(data: ContentCell){
        self.titleLabel.text = data.title
        self.descLabel.text  = data.description
        
    }

}
