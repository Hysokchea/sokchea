//
//  ButtonCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 14/10/22.
//

import UIKit

class ButtonTableCell: UITableViewCell {

    @IBOutlet weak var doneBtn: UIButton!
    var controller = UIViewController()
//    var delegateSelectRoom: SelectRoomDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func selectRoomTap(){
//        delegateSelectRoom?.selectRoom()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.controller.dismiss(animated: true)
    }
}
