//
//  SelectRoomVM.swift
//  WeMeetingSample
//
//  Created by sokchea on 14/10/22.
//

import Foundation

class SelectRoomVM {
    
    var cells : [SelectRoomModel<Any>] = []
    
    func initCell(){
        
        cells = []
        cells.append(SelectRoomModel<Any>(value: ContentCell(title: "Select Room ", description: "There are 4 type of meeting room.") , rowType: .contentCell))
        
        cells.append(SelectRoomModel<Any>(value: PickerRoomCell(valuePicker: ["Angkor room", "jeju", "Siem Reap","Battambong"]), rowType: .pickerRoomCell))
        
        cells.append(SelectRoomModel<Any>(value: ButtonCell(titleBtn: "DONE"), rowType: .buttonCell))
        
    }
}
