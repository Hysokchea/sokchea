//
//  SelectRoomData.swift
//  WeMeetingSample
//
//  Created by sokchea on 14/10/22.
//

import Foundation

enum SelectRoomRowType{
    case contentCell
    case pickerRoomCell
    case buttonCell
}

struct SelectRoomModel <T> {
    var value           : T?
    var rowType         : SelectRoomRowType?
}

struct ContentCell {
    var title           : String = ""
    var description     : String = ""
}

struct PickerRoomCell{
    var valuePicker           : [String]
}

struct ButtonCell{
    var titleBtn        : String
}
