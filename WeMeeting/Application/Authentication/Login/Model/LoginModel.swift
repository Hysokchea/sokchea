//
//  LoginModel.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 6/10/22.
//

import Foundation


enum LoginRowType {
    case HEADER
    case TITLE
    case TEXTFIELD_EMAIL
    case TEXTFIELD_PASSWORD
    case LOGIN
    case SOCIAL
}

struct DataInfo<T>{
    var type : LoginRowType
    var value : T?
}

struct ContentData{
    
    var object : [Object]
    struct Object{
        var imagePrfile     : String?
        var title           : String?
        var subTitle        : String?
        var imageTextfield  : String?
        var placeholder     : String?
        var rowType         : LoginRowType?
        var imageSocial: [ImageObject]?
        
        struct ImageObject{
            var imageGoogle : String?
            var imageNumber : String?
            var rowType         : LoginRowType?
        }
    }
    
}



//
//enum TypeCell: String{
//
//    case HEAD_PROFILE
//    case TITLE
//    case INPUT_TEXTFIELD
//    case LOGIN
//
//}
//
//struct DataInFor<T>{
//    var type : TypeCell
//    var value : T?
//}
//
////ProfileHeader
//struct HeadProfile{
//    var image       : String?
//    var titleLang   : String?
//    var type        : TypeCell?
//}
//
////TitleAndSubtitle
//struct TitleWelcome{
//    var title       : String?
//    var subTitle    : String?
//    var type        : TypeCell?
//}
//
////InputTextField
//struct InputTF {
//    var object          : [Object]
//    struct Object{
//        var image       : String?
//        var placeholder : String?
//        var type        : TypeCell?
//    }
//}
//
////InputTextField
//struct LoginButton{
//    var object : [Object]
//    struct Object{
//        var title : String?
//        var type        : TypeCell?
//    }
//}
//
//struct ValidationLogin{
//    var email : String?
//    var password : String?
//}
//
//
