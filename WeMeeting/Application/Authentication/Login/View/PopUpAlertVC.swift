//
//  PopUpAlertVC.swift
//  WeMeet
//
//  Created by Phanna on 16/10/22.
//

import UIKit

class PopUpAlertVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissBackground(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func dismissButton(_ sender: UIButton) {
        sender.press {
            self.dismiss(animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
