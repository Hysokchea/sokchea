//
//  HeaderProfileCell.swift
//  weMeeting-Project
//


import UIKit

class HeaderProfileCell: UITableViewCell {

    var delegate: LoginViewControllerDelegate?
    
    @IBOutlet weak var langaugeView     : UIView!
    @IBOutlet weak var profileImage     : UIImageView!
    @IBOutlet weak var Languagelabel    : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        profileImage.layer.cornerRadius = 15
        profileImage.clipsToBounds = false
        profileImage.layer.shadowOpacity = 0.3
        profileImage.layer.shadowOffset = CGSize.zero
        profileImage.layer.shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 80, height: 85), cornerRadius: 15).cgPath

        langaugeView.layer.cornerRadius = 15
        langaugeView.clipsToBounds = false
        langaugeView.layer.shadowOpacity = 0.3
        langaugeView.layer.shadowOffset = CGSize.zero
        langaugeView.layer.shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 120, height: 33), cornerRadius: 15).cgPath
        
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(self.languageTapped))
        langaugeView.addGestureRecognizer(tapGR)
        langaugeView.isUserInteractionEnabled = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @objc func languageTapped(sender: UITapGestureRecognizer) {
        
        delegate?.didTap()
         
    }
}
