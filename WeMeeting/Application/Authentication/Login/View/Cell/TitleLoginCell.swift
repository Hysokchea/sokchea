//
//  TitleLoginCell.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 6/10/22.
//

import UIKit

class TitleLoginCell: UITableViewCell {

    @IBOutlet weak var welcomeLabel : UILabel!
    @IBOutlet weak var descLabel    : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
