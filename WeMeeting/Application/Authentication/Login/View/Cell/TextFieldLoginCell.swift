//
//  TextFieldCell.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 6/10/22.
//

import UIKit

class TextFieldLoginCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var txtRequiredLbl: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var viewTextFieldLogin: UIView!
    @IBOutlet weak var imageTF: UIImageView!
    
//    var loginVM = LoginVM.shared
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewTextFieldLogin.layer.cornerRadius = 15
        textField.delegate = self
//        txtRequiredLbl.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
                viewTextFieldLogin.layer.borderWidth = 1
        viewTextFieldLogin.layer.borderColor = UIColor(hexString: "#4474B5").cgColor
        viewTextFieldLogin.backgroundColor = UIColor(hexString: "#EDF5FF")

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewTextFieldLogin.layer.borderWidth = 0
        viewTextFieldLogin.backgroundColor = .white
    }


}
