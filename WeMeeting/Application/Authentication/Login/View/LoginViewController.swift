//
//  LoginViewController.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 3/10/22.
//

import UIKit


protocol LoginViewControllerDelegate{
    
    
    func didTap()
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginwith: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var loginVM = LoginVM()
    var inputTextRow = -1
    var inputTextSection = -1
    var email       = ""
    var password    = ""
    var subtitle            = "Button"
    
    var okAction            : Completion?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginVM.getData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.checkNotify()
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.value(forKey: "alertLanguage") == nil {
            UserDefaults.standard.setValue(true, forKey: "alertLanguage")
            if #available(iOS 16.0, *){
                self.alertChooseLanguage()
            }else{
                
            }
        }
    }
    
    private func checkNotify(){
        MyNotify.listen(self, selector: #selector(reloadLocalize), name: .reloadLocalize)
        
    }
    @objc func reloadLocalize(){
        DispatchQueue.main.async { [self] in
            loginVM.getData()
            tableView.reloadData()
            
        }
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    @IBAction func loginButton(_ sender: Any) {
        
        let cellValidateEmail = self.loginVM.header?.object.firstIndex(where: {$0.rowType == LoginRowType.TEXTFIELD_EMAIL}) ?? 0
        let cellEmail = self.tableView.cellForRow(at: IndexPath(row: cellValidateEmail, section: 0)) as? TextFieldLoginCell
        
        let cellValidateEmail1 = self.loginVM.header?.object.firstIndex(where: {$0.rowType == LoginRowType.TEXTFIELD_PASSWORD}) ?? 0
        let cellEmail1 = self.tableView.cellForRow(at: IndexPath(row: cellValidateEmail1, section: 0)) as? TextFieldLoginCell
        
        
        
        if cellEmail?.textField.text == ""  || cellEmail1?.textField.text == "" {
            
            let storyboard  = UIStoryboard(name: "PopupAlertSB", bundle: nil)
            let myAlert     = storyboard.instantiateViewController(withIdentifier: "PopUpAlertVC") as! PopUpAlertVC
            myAlert.modalPresentationStyle  = UIModalPresentationStyle.overFullScreen
            myAlert.modalTransitionStyle    = UIModalTransitionStyle.crossDissolve;
            self.present(myAlert, animated: true, completion: nil)
        }else{
            if isValidEmail(email: "kirit@gmail.com"){
                print("Validate EmailID")
                print("invalide EmailID")
                let storyboard = UIStoryboard(name: "MainSB", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MainVC")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            }
            else{
                let storyboard  = UIStoryboard(name: "PopupAlertSB", bundle: nil)
                let myAlert     = storyboard.instantiateViewController(withIdentifier: "PopUpAlertVC") as! PopUpAlertVC
                myAlert.modalPresentationStyle  = UIModalPresentationStyle.overFullScreen
                myAlert.modalTransitionStyle    = UIModalTransitionStyle.crossDissolve;
                self.present(myAlert, animated: true, completion: nil)
                
                
            }
            
        }
        
    }
}

extension LoginViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loginVM.header?.object.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        let rowType = loginVM.header?.object[indexPath.row].rowType
        switch rowType {
        case .HEADER :
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderProfileCell", for: indexPath) as! HeaderProfileCell
            cell.profileImage.image = UIImage(named: loginVM.header?.object[indexPath.row].imagePrfile ?? "")
            cell.Languagelabel.text = loginVM.header?.object[indexPath.row].title
            cell.delegate = self
            
            return cell
        case .TITLE :
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleLoginCell", for: indexPath) as! TitleLoginCell
            cell.welcomeLabel.text  =  loginVM.header?.object[indexPath.row].title
            cell.descLabel.text     =  loginVM.header?.object[indexPath.row].subTitle
            return cell
        case .TEXTFIELD_EMAIL:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldLoginCell", for: indexPath) as! TextFieldLoginCell
            cell.imageTF.image = UIImage(named: loginVM.header?.object[indexPath.row].imageTextfield ?? "")
            cell.textField.placeholder = loginVM.header?.object[indexPath.row].placeholder
            
            //            cell.textField.delegate = self
            cell.textField.tag = indexPath.row
            return cell
        case .TEXTFIELD_PASSWORD:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldLoginCell", for: indexPath) as! TextFieldLoginCell
            cell.imageTF.image = UIImage(named: loginVM.header?.object[indexPath.row].imageTextfield ?? "")
            cell.textField.placeholder = loginVM.header?.object[indexPath.row].placeholder
           
            cell.textField.tag = indexPath.row
            return cell
        case .LOGIN:
            let cell    = tableView.dequeueReusableCell(withIdentifier:             "buttonCell", for: indexPath) as! buttonCell
            cell.buttonLogin.setTitle(loginVM.header?.object[indexPath.row].title, for: .normal)
            cell.owner  = self
            return cell
        case .SOCIAL:
            let cell    = tableView.dequeueReusableCell(withIdentifier:             "SocialCell", for: indexPath) as! SocialCell
            cell.descLabel.text = loginVM.header?.object[indexPath.row].title
            return cell
            
        case .none:
            return UITableViewCell()
        }
    }
    
}

@available(iOS 16.0, *)
extension LoginViewController: LoginViewControllerDelegate{
    
    func didTap() {
        
        self.alertChooseLanguage()
    }
}

extension LoginViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 0 {
            email = textField.text ?? ""
        }
        
        if textField.tag == 1 {
            password = textField.text ?? ""
        }
        
        return true
    }
}

extension LoginViewController {
    func initialize(subtitle: String,okAction: Completion?){
        self.subtitle   = subtitle
        self.okAction   = okAction
    }
}
