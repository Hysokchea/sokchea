//
//  LoginVM.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 6/10/22.
//

import Foundation
import UIKit

class LoginVM{
//    static let shared = LoginVM()
//
//    private init(){
//
//    }
    
    var contentData = [DataInfo<Any>]()
    var header : ContentData?
    
    func getData(){
      header = ContentData(object: [
        
        ContentData.Object(imagePrfile: "icon-kosign",
                           title: "english".localized,
                           rowType: .HEADER
                           ),
        
        ContentData.Object(title: "welcome_back".localized,
                           subTitle: "subwelcome_back".localized,
                           rowType: .TITLE
                           ),
        
        ContentData.Object(imageTextfield: "icon-email",
                           placeholder: "email_placeholder".localized,
                           rowType: .TEXTFIELD_EMAIL
                          ),
        
        ContentData.Object(imageTextfield: "icon-password",
                           placeholder: "password_placeholder".localized,
                           rowType: .TEXTFIELD_PASSWORD
                          ),
        
        ContentData.Object(title: "login".localized,
                           rowType: .LOGIN
                           ),
        ContentData.Object(title: "login_with".localized,
                           rowType: .SOCIAL,
                           imageSocial: [
                            ContentData.Object.ImageObject(
                                imageGoogle: "icon-password",
                                imageNumber: "icon-email")
                           ]),
      ])
    }
    
}



//
//class LoginVM{
//
//    static let shared = LoginVM()
//
//    private init(){
//
//    }
//
//    var emailpassword : ValidationLogin?
//
//    var myDataInfo       = [DataInFor<Any>]()
//    var headProfile      = [HeadProfile]()
//    var titleWelcome     = [TitleWelcome]()
//    var email            : InputTF?
//    var password         : InputTF?
////    var loginButton      = [LoginButton]()
//    var loginButton: LoginButton?
//
//    func getLoginData(){
//
//        headProfile = [
//            HeadProfile(
//                image       : "icon-kosign",
//                titleLang   : "english".localized,
//                type        : .HEAD_PROFILE
//            )
//        ]
//
//        titleWelcome = [
//            TitleWelcome(
//                title       : "welcome_back".localized,
//                subTitle    : "subwelcome_back".localized,
//                type        : .TITLE
//            )
//        ]
//
//        email = InputTF(
//            object: [
//                InputTF.Object(image: "icon-email", placeholder: "email_placeholder".localized, type: .INPUT_TEXTFIELD),
//                InputTF.Object(image: "icon-password", placeholder: "password_placeholder".localized, type: .INPUT_TEXTFIELD)
//            ])
//
//        loginButton = LoginButton(
//            object: [
//                LoginButton.Object(title: "login_btn".localized, type: .LOGIN),
//            ]
//        )
//
//        myDataInfo = [
//            DataInFor(type: .HEAD_PROFILE,      value: headProfile),
//            DataInFor(type: .TITLE,             value: titleWelcome),
//            DataInFor(type: .INPUT_TEXTFIELD,   value: email),
//            DataInFor(type: .LOGIN,             value: loginButton),
//
//        ]
//    }
//}
