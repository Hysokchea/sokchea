//
//  ChooseLanguageVM.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 9/10/22.
//

import Foundation
import UIKit

class LanguageVM {
    var language:  [LanguageData] = []
    
    func getDataInfo(){
        language = [
            LanguageData(language: "Khmer", image: "icon-Cambodia",  isSelect: Shared.language == .Khmer),
            LanguageData(language: "Korea", image: "icon-korea",  isSelect: Shared.language == .Korean),
            LanguageData(language: "USA",   image: "icon-eng", isSelect: Shared.language == .English)
        ]
    }
}
