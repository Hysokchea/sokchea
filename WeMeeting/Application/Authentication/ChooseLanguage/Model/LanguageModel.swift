//
//  LanguageModel.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 8/10/22.
//

import Foundation


struct LanguageData{
    
    let language    : String
    let image       : String
    let isSelect    : Bool
}
