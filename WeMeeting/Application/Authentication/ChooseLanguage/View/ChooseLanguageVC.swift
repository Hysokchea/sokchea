//
//  ChooseLanguageVC.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 9/10/22.
//

import UIKit

class ChooseLanguageVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subTitleLang: UILabel!
    @IBOutlet weak var titleLang: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    var languageVM = LanguageVM()
        var dataSource : [String]   = ["ភាសាខ្មែរ", "한국어", "English"]
        let lang                    = ["km", "ko", "en"]
        var selectedRow             = 2
        var index                   = 0
        var isChoose                : Bool = false
       

        override func viewDidLoad() {
            super.viewDidLoad()
            
            tableView.allowsSelection = true
            self.tableView.delegate     = self
            self.tableView.dataSource   = self
            languageVM.getDataInfo()
           
           
            let langCode            = MyDefaults.get(key: .appLang) as? String ?? ""
            self.selectedRow        = langCode == "kh" ? 0 : langCode == "ko" ? 1 : langCode == "en" ? 2 : 0
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            btnSelect.setTitle("btn_select".localized, for: .normal)
            titleLang.text = "display_language".localized
            subTitleLang.text = "subtitle_display_language".localized
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            btnSelect.setTitle("btn_select".localized, for: .normal)
            titleLang.text = "display_language".localized
            subTitleLang.text = "subtitle_display_language".localized
            
        }

        
        @IBAction func selectChangeBtn(_ sender: UIButton) {
            sender.press {
                if self.isChoose {
                    self.selectedRow = self.index
                    guard let lang = LanguageCode(rawValue: self.lang[self.index]) else {return }
                    Shared.language = lang
                    MyDefaults.set(key: .appLang, value: lang.rawValue)
                    MyNotify.send(name: .reloadLocalize)
                }else{
                    guard let lang = LanguageCode(rawValue: self.lang[self.selectedRow]) else {return }
                    Shared.language = lang
                    MyDefaults.set(key: .appLang, value: lang.rawValue)
                    MyNotify.send(name: .reloadLocalize)
                }
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }

    extension ChooseLanguageVC: UITableViewDelegate, UITableViewDataSource{
        
        //count row in section
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return languageVM.language.count
        }
        
        // Returns a cell to insert in a particular location of the table view.
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AllLanguageCell", for: indexPath) as! AllLanguageCell
//save current language and bacl
            if languageVM.language[indexPath.row].isSelect {
                cell.contentView.backgroundColor = UIColor(hexString: "#EDF5FF")
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            cell.configCellLanguage(title       : dataSource[indexPath.row],
                                    imageStr    : languageVM.language[indexPath.row])
            return cell
        }
        
        //did select at row for choose language
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                let selectionCell = tableView.cellForRow(at: indexPath) as! AllLanguageCell
             selectionCell.contentView.backgroundColor = UIColor(hexString: "#EDF5FF")
            index = indexPath.row
            isChoose = true

            }

            func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
                let selectionCell = tableView.cellForRow(at: indexPath) as! AllLanguageCell

                    selectionCell.contentView.backgroundColor = UIColor(named: "#0075E3") //Clear color

            }

    }

@available(iOS 16.0, *)
extension UIViewController {
    func alertChooseLanguage(){
        let storyboard = UIStoryboard(name: "LanguageSB", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChooseLanguageVC")
        
        
        if #available(iOS 15.0, *) {
            if let sheet        = vc.sheetPresentationController {
                let smallId     = UISheetPresentationController.Detent.Identifier("small")
                let smallDetent = UISheetPresentationController.Detent.custom(identifier: smallId) { context in
                    return 340
                }
                
                sheet.detents = [smallDetent, .large()]
                sheet.largestUndimmedDetentIdentifier = .medium
                
            }
            present(vc, animated: true, completion: nil)
        }
    }
}
