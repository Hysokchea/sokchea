//
//  AllLanguageCell.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 9/10/22.
//

import UIKit

class AllLanguageCell: UITableViewCell {

    @IBOutlet weak var flagImg: UIImageView!
    
    @IBOutlet weak var selectIcon: UIButton!
    
    @IBOutlet weak var languageLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            selectIcon.isHidden = !selected
            // Configure the view for the selected state
        }
       
        func configCellLanguage(title: String, imageStr: LanguageData){
            languageLbl.text = title
            flagImg.image = UIImage(named: imageStr.image)
        }//, isSelected: Bool
   
    

}
