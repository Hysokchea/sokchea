//
//  RandomLaunchScreenVC.swift
//  weMeeting-Project
//
//  Created by Poem Kimlang on 6/10/22.
//

import UIKit

@available(iOS 16.0, *)
class RandomLaunchScreenVC: UIViewController {

    @IBOutlet weak var copyright: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var getStartedBtn    : UIButton!
    
    
   
    var images: [String] = [
        "together",
        "meeting",
        "room",
        "floor",
        "explain",
        "smile"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkRandom()
        getStartedBtn.layer.cornerRadius = 15
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        createTimer()
        getStartedBtn.setTitle("get_started".localized, for: .normal)
        copyright.text = "copy_right".localized
    }
    override func viewWillAppear(_ animated: Bool) {
        getStartedBtn.setTitle("get_started".localized, for: .normal)
        self.checkNotify()
        getStartedBtn.setTitle("get_started".localized, for: .normal)
        copyright.text = "copy_right".localized
        
    }
    
    func checkRandom(){
//        imageView.image = images.randomElement() as? UIImage
        let randomImage = images.randomElement()
        let currentDate = Date()
        let dateFormart = DateFormatter()
//        dateFormart.timeStyle = .medium
        dateFormart.dateStyle = .long
        
        let dateTimeString = dateFormart.string(from: currentDate)
        print("==> " + dateTimeString)
        
        let day = currentDate.dateFormat().split(separator: "/").first
        
        guard let randomUserDefault = UserDefaults.standard.object(forKey: "RandomImage") as? [String]
        else {
            
           return UserDefaults.standard.setValue([randomImage ?? "", day ?? ""], forKey: "RandomImage")
            
        }

        if randomUserDefault.contains(where: { $0 == "17" }) {
            
            imageView.image = UIImage(named: randomUserDefault.first ?? "")
        }
        else {
            
        }
    
    }
    
    private func checkNotify(){
        MyNotify.listen(self, selector: #selector(reloadLocalize), name: .reloadLocalize)
    }
    @objc func reloadLocalize(){
        DispatchQueue.main.async {[self] in
            getStartedBtn.setTitle("get_started".localized, for: .normal)
            copyright.text = "copy_right".localized
            
        }
    }
    
    @available(iOS 16.0, *)
    @IBAction func goToLoginBtn(_ sender: UIButton) {
        
         
        sender.press {
            print("Go to login screen")
            

            let storyboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(vc, animated: true)
            
        }
      
    }
    
//    func createTimer(){
//        let timer = Timer.scheduledTimer(
//            timeInterval    : 5, //60s* 60m *24h = 1day
//            target          : self,
//            selector        : #selector(fireTimer),
//            userInfo        : nil,
//            repeats         : true)
////        let date = Date()
////        let calendar = Calendar.current
////        print(calendar)
//    }
//
//    @objc func fireTimer(){
//        imageView.image = images.randomElement() as? UIImage
//    }

}
