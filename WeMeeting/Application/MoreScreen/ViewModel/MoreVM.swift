//
//  MoreVM.swift
//  WeMeetingSample
//
//  Created by Un Bora on 5/10/22.
//

import Foundation

class MoreVM {
    
    var moreData = [MoreModel]()
    
    func fetchSettingData(){
        self.moreData = [
            MoreModel(title: "Welcome Back!", leftDesc: "Monoion", rightDesc: "", iconLeft: "explain", iconRigth: "", rowType: .profile),
            
            MoreModel(title: "Settings", leftDesc: "", rightDesc: "", iconLeft: "", iconRigth: "", rowType: .rowTitle),
            
            MoreModel(title: "Languuage", leftDesc: "Choose you beloved", rightDesc: "🇰🇭Cambodia", iconLeft: "globe", iconRigth: "arrow-goon", rowType: .labelAndButton),
            
            MoreModel(title: "Notifications", leftDesc: "Turn on Notification to get Notified", rightDesc: "", iconLeft: "bell", iconRigth: "on-switch", rowType: .notification),
            
            MoreModel(title: "Local Notifications", leftDesc: "Turn it on if you don't want to be late", rightDesc: "", iconLeft: "bell", iconRigth: "on-switch", rowType: .notification),
            
            MoreModel(title: "About App", leftDesc: "", rightDesc: "", iconLeft: "", iconRigth: "", rowType: .rowTitle),
            
            MoreModel(title: "About Us", leftDesc: "23hours we will response to you", rightDesc: "", iconLeft: "company", iconRigth: "arrow-goon", rowType: .aboutUs),
            
            MoreModel(title: "App Version", leftDesc: "We still kepping updating", rightDesc: "v 1.1.0", iconLeft: "info", iconRigth: "", rowType: .Label),
            
            MoreModel(title: "App Version", leftDesc: "We still kepping updating", rightDesc: "", iconLeft: "", iconRigth: "", rowType: .logoutbtn),
            
        ]
    }
}
