//
//  MoreModel.swift
//  WeMeetingSample
//
//  Created by Un Bora on 13/10/22.
//

import Foundation

enum RowType {
    case rowTitle
    case labelAndButton
    case profile
    case notification
    case Label
    case aboutUs
    case Button
    case logoutbtn
}

struct MoreModel {
    let title       : String
    let leftDesc    : String
    let rightDesc   : String
    let iconLeft    : String
    let iconRigth   : String
    let rowType     : RowType
}
