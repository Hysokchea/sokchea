//
//  SettingVC.swift
//  WeMeetingSample
//
//  Created by Un Bora on 7/10/22.
//

import UIKit

class MoreVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var moreVM = MoreVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        ======== xib for user ========
        let myCellTesting = UINib(nibName: "CreateByScreenTableViewCell", bundle: nil)
        tableView.register(myCellTesting, forCellReuseIdentifier: "CreateByScreenTableViewCell")
        self.moreVM.fetchSettingData()

    }
}

extension MoreVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.moreVM.moreData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowType = self.moreVM.moreData[indexPath.row].rowType
        
        switch rowType {
        case .labelAndButton:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell", for: indexPath) as! LanguageTableViewCell
            cell.configCell(data: self.moreVM.moreData[indexPath.row])
            cell.switchbtn.isHidden = true
            return cell
        case .Button:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell", for: indexPath) as! LanguageTableViewCell
            cell.configCell(data: self.moreVM.moreData[indexPath.row])
            cell.countryName.isHidden = true
            cell.iconRight.isHidden = true
            return cell
        case .Label:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell", for: indexPath) as! LanguageTableViewCell
            cell.configCell(data: self.moreVM.moreData[indexPath.row])
            cell.countryName.font = UIFont(name: "Rubik-Bold", size: 16)
            cell.countryName.textColor = UIColor(red: 68/255, green: 116/255, blue: 181/255, alpha: 1.0)
            cell.iconRight.isHidden = true
            cell.switchbtn.isHidden = true
            return cell
        case .notification:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell", for: indexPath) as! LanguageTableViewCell
            cell.configCell(data: self.moreVM.moreData[indexPath.row])
            cell.iconRight.isHidden = true
            return cell
            
        case .profile:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            cell.configCell(data: self.moreVM.moreData[indexPath.row])
            return cell
        case .logoutbtn:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LogOutTableViewCell", for: indexPath)
            return cell
        case .aboutUs:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell", for: indexPath) as! LanguageTableViewCell
            cell.configCell(data: self.moreVM.moreData[indexPath.row])
            cell.countryName.isHidden = true
            cell.switchbtn.isHidden = true
            return cell
            
        case .rowTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SectionNameTableViewCell", for: indexPath) as! SectionNameTableViewCell
            cell.configCell(data: self.moreVM.moreData[indexPath.row])
            return cell
        }
        
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return moreVM.moreData[section].sectionTitle
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 20
//    }
    
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
//    {
//        let header = view as! UITableViewHeaderFooterView
//        header.textLabel?.font = UIFont(name: "Rubik-Bold", size: 20)
////        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 24)
//        header.textLabel?.textColor = UIColor(red: 68/255, green: 116/255, blue: 181/255, alpha: 1.0)
//
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

