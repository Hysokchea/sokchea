//
//  LanguageTableViewCell.swift
//  WeMeetingSample
//
//  Created by Un Bora on 7/10/22.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var languageTitle: UILabel!
    @IBOutlet weak var lamguageDesc: UILabel!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var switchbtn: UISwitch!
    @IBOutlet weak var iconRight: UIButton!
    @IBOutlet weak var iconLeft: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data: MoreModel){
        let imgLeft = data.iconLeft
    
        self.iconLeft.image = UIImage(named: "\(imgLeft)")
    
        self.languageTitle.text = data.title
        self.lamguageDesc.text = data.leftDesc
        self.countryName.text = data.rightDesc
        switchbtn.transform = CGAffineTransformMakeScale(0.54, 0.54)
    }
    

}
