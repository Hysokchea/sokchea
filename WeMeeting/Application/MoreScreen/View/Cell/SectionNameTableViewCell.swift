//
//  SectionNameTableViewCell.swift
//  WeMeetingSample
//
//  Created by Un Bora on 16/10/22.
//

import UIKit

class SectionNameTableViewCell: UITableViewCell {

    @IBOutlet weak var sectionTitle: UILabel!
    func configCell(data: MoreModel){
        sectionTitle.text = data.title
    }
}
