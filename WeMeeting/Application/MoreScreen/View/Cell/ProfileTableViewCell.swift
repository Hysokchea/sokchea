//
//  ProfileTableViewCell.swift
//  WeMeetingSample
//
//  Created by Un Bora on 7/10/22.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var profileBio: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configCell (data: MoreModel){
        let img = data.iconLeft
        self.profileImg.image = UIImage(named: "\(img)")
        profileImg.layer.cornerRadius    = 65/2
        profileImg.clipsToBounds         = true
        profileImg.translatesAutoresizingMaskIntoConstraints = false
        self.profileBio.text = data.title
        self.name.text = data.leftDesc
        
    }


}
