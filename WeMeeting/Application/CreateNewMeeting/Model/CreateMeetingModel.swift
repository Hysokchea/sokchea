//
//  CreateMeetingModel.swift
//  WeMeetingSample
//
//  Created by sokchea on 5/10/22.
//

import Foundation

enum CreateMeetingRowType{
    case dropdownCell
    case startAndEndTimeCell
    case inputDescriptionCell
    case setMeetingColorCell
    case advanceOptionalCell
    case repeatCell
//    case profileMeetingCell
}

enum CreateMeetingIsEmptyRow {
    case Yes
    case No
}

enum DropdownCellType{
    case haftScreen
    case dropdown
}

enum RepeatEndType {
    case Never
    case On
    case After
}

struct CreateNewMeetingModel <T> {
    var value           : T?
    var rowType         : CreateMeetingRowType
}

struct DropdownCell {
    var title           : String
    var value           : String
    var placeholder     : String
    var leftIconImg     : String
    var rightBtn        : String
    var cellType        : DropdownCellType
    var isHiddenBtn     : Bool = false
    
}

struct StartAndEndTimeCell {
    var title                   : String
    var startTime               : String
    var endTime                 : String
    var startTimePlaceholder    : String
    var endTimePlaceholder      : String
    var leftIconImg             : String
    var rightIconImg            : String

}

struct SetMeetingColorCell {
    var title               : String
    var noteColorTitle      : String
    var colorName           : [String]
    var selectedColor       : String
    var selectedIndex       : Int
    var leftIcon            : String
}

struct AdvanceOptionalCell {
    var title               : String
    var rightIcon           : String
}

struct RepeatCell{
    var repeatTitle         : String
    var numberOfTime        : String
    var numberOfWeek        : String
    var rightIcon           : String
    var endTitle            : String
    var selectedEndTime     : RepeatEndType
    var repeatEndValue      : String
}

struct DescriptionCell {
    var title               : String
    var placeholder         : String
    var inputTextValue      : String
    var leftIcon            : String
}


        

    


