//
//  CreateMeetingVM.swift
//  WeMeetingSample
//
//  Created by sokchea on 5/10/22.
//

import Foundation

class CreateNewMeetingVM {

    var cells : [CreateNewMeetingModel<Any>] = []
    var isCollapsed = false
    
    func initCell() {
        
        cells = []
    
//        cells.append(CreateNewMeetingModel<Any>(value: DropdownCell(title: "Create by", value: "Sokchea", placeholder: "sokcheahy@gmail.com", leftIconImg: "panda", rightBtn: "chevron-down", cellType: .haftScreen, isHiddenBtn: false), rowType: .profileMeetingCell))
        
        cells.append(CreateNewMeetingModel<Any>(value: DropdownCell(title: "Select Room : ", value: " ", placeholder: "Please select", leftIconImg: "door", rightBtn: "chevron-down", cellType: .haftScreen, isHiddenBtn: false), rowType: .dropdownCell))
            
        cells.append(CreateNewMeetingModel<Any>(value: DropdownCell(title: "Input Meeting Agenda", value: "", placeholder: "Please input", leftIconImg: "input", rightBtn: "", cellType: .haftScreen, isHiddenBtn: true), rowType: .dropdownCell))
        
        cells.append(CreateNewMeetingModel<Any>(value: DropdownCell(title: "Select Reserved Date :", value: "", placeholder: "Please select", leftIconImg: "input", rightBtn: "chevron-down", cellType: .haftScreen, isHiddenBtn: true), rowType: .dropdownCell))
        
            cells.append(CreateNewMeetingModel<Any>(value: StartAndEndTimeCell(title: "Input Start & End Meeting Period :", startTime: " ", endTime: " ", startTimePlaceholder: "Start time", endTimePlaceholder: "End time", leftIconImg: "alarm", rightIconImg: "alarm"), rowType: .startAndEndTimeCell))
        
            cells.append(CreateNewMeetingModel<Any>(value: SetMeetingColorCell(title: "Set Meeting Color :", noteColorTitle: "Note Color", colorName: ["#38761d","#f8d708","#a8763e","#38761d","#f8d708","#a8763e","#38761d","#f8d708","#a8763e","#a8763e"], selectedColor: "", selectedIndex: 2, leftIcon: "alarm"),rowType: .setMeetingColorCell))
            
            cells.append(CreateNewMeetingModel<Any>(value: AdvanceOptionalCell(title: "Advance ( Optional )", rightIcon: "chevron-down"), rowType: .advanceOptionalCell))
        
      
        
     
        
    }
    
}
