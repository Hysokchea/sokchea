//
//  SetMeetingColorCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 9/10/22.
//

import UIKit

class SetMeetColorCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noteColorLabel: UILabel!
    @IBOutlet weak var leftIcon: UIImageView!
    
    var createMeetingVM = CreateNewMeetingVM()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        containerView.layer.cornerRadius = 15
        
        createMeetingVM.initCell()
        
        let cellNib = UINib(nibName: "SetMeetingColorCollectionCellSB", bundle: nil)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: "SetMeetingColorCollectionCell")
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: 49, height: 48)
        flowLayout.minimumInteritemSpacing = 14.0
        flowLayout.minimumLineSpacing = 14.0
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.showsHorizontalScrollIndicator = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(data: SetMeetingColorCell){
        self.titleLabel.text = data.title
        self.noteColorLabel.text = data.noteColorTitle
        self.leftIcon.image = UIImage(named: data.leftIcon)
    }
}

extension SetMeetColorCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let data = self.createMeetingVM.cells[4]
        return (data.value as! SetMeetingColorCell).colorName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let data = self.createMeetingVM.cells[4]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetMeetingColorCollectionCell", for: indexPath) as! SetMeetingColorCollectionCell
        let color = (data.value as! SetMeetingColorCell).colorName
        cell.colorView.backgroundColor = UIColor(hexString: color[indexPath.item])
        cell.trueImage.isHidden = (data.value as! SetMeetingColorCell).selectedIndex != indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var createMeetingCell = createMeetingVM.cells[4]
        var setMeetingColorCell = createMeetingCell.value as! SetMeetingColorCell
        setMeetingColorCell.selectedIndex = indexPath.row
        createMeetingCell.value = setMeetingColorCell
        createMeetingVM.cells[4] = createMeetingCell
        collectionView.reloadData()
    }
  
}
