//
//  SetMeetingColorCollectionCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 8/10/22.
//

import UIKit

class SetMeetingColorCollectionCell: UICollectionViewCell {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var trueImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        colorView.layer.cornerRadius = 15
    }
}
