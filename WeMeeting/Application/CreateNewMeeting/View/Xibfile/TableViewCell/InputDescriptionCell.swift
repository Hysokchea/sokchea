//
//  InputDescriptionCellTableViewCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 16/10/22.
//

import UIKit

class InputDescriptionCell: UITableViewCell {

    var completion : () -> Void = {}
    
    @IBOutlet weak var titleLabel       : UILabel!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var leftIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textView.delegate = self
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configCell(data: DescriptionCell){
        self.titleLabel.text = data.title
        self.leftIcon.image = UIImage(named: data.leftIcon)
//        self.textView.delegate
        
    }

}

extension InputDescriptionCell: UITextViewDelegate {
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        completion()
        return true
    }
}
