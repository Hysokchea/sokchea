//
//  StartEndTimeCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 8/10/22.
//

import UIKit

class StartEndTimeCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var startTimeContainerView: UIView!
    @IBOutlet weak var startTimeTextField: UITextField!
    
    @IBOutlet weak var endTimeContainerView: UIView!
    @IBOutlet weak var endTimeTextField: UITextField!
    @IBOutlet weak var leftIcon: UIImageView!
    @IBOutlet weak var rightIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        startTimeContainerView.layer.cornerRadius = 15
        endTimeContainerView.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
    }
    
    func configCell(data: StartAndEndTimeCell){
        self.titleLabel.text = data.title
        self.startTimeTextField.placeholder = data.startTimePlaceholder
        self.leftIcon.image = UIImage(named: data.leftIconImg)
        self.endTimeTextField.placeholder = data.endTimePlaceholder
        self.rightIcon.image = UIImage(named: data.rightIconImg)
    }
    
}
