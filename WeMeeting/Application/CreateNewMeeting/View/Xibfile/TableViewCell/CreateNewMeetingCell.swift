//
//  CreateNewMeetingCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 6/10/22.
//

import UIKit

class CreateNewMeetingCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel           : UILabel!
    @IBOutlet weak var containerView        : UIView!
    @IBOutlet weak var roomTextField        : UITextField!
    @IBOutlet weak var leftIcon             : UIImageView!
    @IBOutlet weak var rigthBtn             : UIButton!
   
    var delegateSelectRoom: SelectRoomDelegate?
    
    var controller = UIViewController()
    
    var isDropdown = false
    
    let dropdown = UITableView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 15
        
        let tapSelectRoom = UITapGestureRecognizer(target: self, action: #selector(self.selectRoomTap))
                rigthBtn.addGestureRecognizer(tapSelectRoom)
                rigthBtn.isUserInteractionEnabled = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configCell(data: DropdownCell){
        self.titleLabel.text = data.title
        self.roomTextField.placeholder = data.placeholder
        self.leftIcon.image = UIImage(named: data.leftIconImg)
        self.rigthBtn.setImage(UIImage(named: data.rightBtn) , for: .normal)
        self.rigthBtn.setTitle("", for: .normal)
        self.rigthBtn.isHidden = data.isHiddenBtn
//        self.roomTextField.text = data.value
    }
    
    @objc func selectRoomTap(){
        delegateSelectRoom?.selectRoom()
    }
    
    @IBAction func setRecurrenceBtn(_ sender: Any) {
        
        if isDropdown{

        } else{
        
            let storyboard = UIStoryboard(name: "SelectRoomSB", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "SelectRoomVC")
                        vc.modalPresentationStyle = .fullScreen
                    self.controller.present(vc, animated: true)
        }
    }
}
