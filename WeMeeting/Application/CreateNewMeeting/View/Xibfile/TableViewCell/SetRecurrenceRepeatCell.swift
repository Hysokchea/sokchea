//
//  SetRecurrenceCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 11/10/22.
//

import UIKit

class SetRecurrenceRepeatCell: UITableViewCell {
    
    @IBOutlet weak var timeWeekTitle        : UILabel!
    @IBOutlet weak var timeTF               : UITextField!
    @IBOutlet weak var timeBtn              : UIButton!
    @IBOutlet weak var weekTF               : UITextField!
    @IBOutlet weak var weekBtn              : UIButton!
    @IBOutlet weak var dayTitle             : UILabel!
    @IBOutlet weak var stackView            : UIStackView!
    @IBOutlet weak var endTitle             : UILabel!
    @IBOutlet weak var neverTitle           : UILabel!
    @IBOutlet weak var onTitle              : UILabel!
    @IBOutlet weak var afterTitle           : UILabel!
    @IBOutlet weak var neverBtn             : UIButton!
    @IBOutlet weak var onBtn                : UIButton!
    @IBOutlet weak var afterBtn             : UIButton!
    @IBOutlet weak var onDateTimeBtn        : UIButton!
    @IBOutlet weak var onDateTimeTF         : UITextField!
    @IBOutlet weak var afterTimeTF          : UITextField!
    @IBOutlet weak var afterTimeBtn         : UIButton!
    @IBOutlet weak var timeIcon: UIImageView!
    @IBOutlet weak var afterIcon: UIImageView!
    @IBOutlet weak var timeDateIcon: UIImageView!
    @IBOutlet weak var weekIcon: UIImageView!
    @IBOutlet weak var occurrenceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(data: RepeatCell) {
        self.timeWeekTitle.text = data.repeatTitle
        self.dayTitle.text = data.repeatTitle
        self.timeTF.text = data.numberOfTime
        self.weekTF.text = data.numberOfWeek
        self.endTitle.text = data.endTitle
        self.timeIcon.image = UIImage(named: data.rightIcon)
        self.weekIcon.image = UIImage(named: data.rightIcon)
        self.timeDateIcon.image = UIImage(named: data.rightIcon)
        self.afterIcon.image = UIImage(named: data.rightIcon)
    
        
    }
    
}
