//
//  AdvanceCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 11/10/22.
//

import UIKit

class AdvanceCell: UITableViewCell {

    @IBOutlet weak var AdvanceBtn: UIButton!
    @IBOutlet weak var advanceLabel: UILabel!
    
    var isCollapsed = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func advanceBtnAction(_ sender: Any) {
        if isCollapsed {
            isCollapsed = false
            AdvanceBtn.setImage(UIImage(named: "chevron-down"), for: .normal)
        } else {
            isCollapsed = true
            AdvanceBtn.setImage(UIImage(named: "chevron-up"), for: .normal)
    
        }
            
    }
    
    func configCell(data: AdvanceOptionalCell){
        self.advanceLabel.text = data.title

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
