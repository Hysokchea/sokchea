//
//  MeetingProfileCell.swift
//  WeMeetingSample
//
//  Created by sokchea on 16/10/22.
//

import UIKit

class MeetingProfileCell: UITableViewCell {

    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var createByLabel: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
   
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configCell(data: DropdownCell) {
        self.profileImage.image = UIImage(named: data.leftIconImg)
        self.profileName.text = data.title
        self.createByLabel.text = data.title
        self.emailLabel.text = data.placeholder
    }
    
}
