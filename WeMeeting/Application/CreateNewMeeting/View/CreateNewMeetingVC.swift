//
//  CreateNewMeetingVC.swift
//  WeMeetingSample
//
//  Created by sokchea on 6/10/22.
//

import UIKit

protocol SelectRoomDelegate{
    func selectRoom()
}


class CreateNewMeetingVC: UIViewController, UISheetPresentationControllerDelegate{

    @IBOutlet weak var tableView: UITableView!
    
    var createMeetingVM = CreateNewMeetingVM()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "CreateNewMeetingCell", bundle: nil), forCellReuseIdentifier: "CreateNewMeetingCell")
        
        tableView.register(UINib(nibName: "StartEndTimeCell", bundle: nil), forCellReuseIdentifier: "StartEndTimeCell")
        
        tableView.register(UINib(nibName: "SetMeetColorCell", bundle: nil), forCellReuseIdentifier: "SetMeetColorCell")
        
        tableView.register(UINib(nibName: "AdvanceOptionalCell", bundle: nil), forCellReuseIdentifier: "AdvanceOptionalCell")
        
        tableView.register(UINib(nibName: "SetRecurrenceRepeatCell", bundle: nil), forCellReuseIdentifier: "SetRecurrenceRepeatCell")
        
        tableView.register(UINib(nibName: "InputDescriptionCell", bundle: nil), forCellReuseIdentifier: "InputDescriptionCell")
        
        //set data from VM
        createMeetingVM.initCell()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @objc func advanceCollapeBtn(){
        if !createMeetingVM.isCollapsed {
            createMeetingVM.isCollapsed = !createMeetingVM.isCollapsed
            
            createMeetingVM.cells.append(CreateNewMeetingModel<Any>(value: DropdownCell(title: " Set Recurrence : ", value: " My Custom ", placeholder: "Please select ", leftIconImg: "collape", rightBtn: "chevron-down", cellType: .dropdown, isHiddenBtn: true), rowType: .dropdownCell))
                 
            createMeetingVM.cells.append(CreateNewMeetingModel<Any>(value: RepeatCell(repeatTitle: "Repeat Every : ", numberOfTime: "1 ", numberOfWeek: "2 weeks ", rightIcon: "chevron-down",  endTitle: "End : ", selectedEndTime: .Never, repeatEndValue: "1"), rowType: .repeatCell))
            
            createMeetingVM.cells.append(CreateNewMeetingModel<Any>(value: DescriptionCell(title: "Input description", placeholder: " ", inputTextValue: "", leftIcon: "document"), rowType: .inputDescriptionCell))

            UIView.transition(with: tableView, duration: 0.3, options: .transitionCrossDissolve, animations: {self.tableView.reloadData()}, completion: nil)

        } else {
            createMeetingVM.isCollapsed = !createMeetingVM.isCollapsed

            let rang = createMeetingVM.cells.count - 1
            let rang2 = createMeetingVM.cells.count - 3
            createMeetingVM.cells.removeSubrange(rang2...rang)

            let index1 = IndexPath(row: rang, section: 0)
            let index2 = IndexPath(row: rang2, section: 0)
            self.tableView.deleteRows(at: [index1, index2 ], with: .automatic)

        }
    }
}

extension CreateNewMeetingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createMeetingVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.createMeetingVM.cells[indexPath.row]
        
        switch data.rowType {
        case.dropdownCell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreateNewMeetingCell", for: indexPath) as! CreateNewMeetingCell
            let value = data.value as! DropdownCell
            cell.configCell(data: value)
            
            switch value.cellType{
            case .haftScreen:
                cell.isDropdown = false
                cell.delegateSelectRoom = self
            case .dropdown:
                cell.isDropdown = true
            }
            return cell
            
        case .startAndEndTimeCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "StartEndTimeCell", for: indexPath) as! StartEndTimeCell
            let value = data.value as! StartAndEndTimeCell
            cell.configCell(data: value)
            return cell
            
        case .setMeetingColorCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SetMeetColorCell", for: indexPath) as! SetMeetColorCell
            let value = data.value as! SetMeetingColorCell
            cell.configCell(data: value)
            return cell
            
        case .advanceOptionalCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdvanceOptionalCell", for: indexPath) as! AdvanceCell
            let value = data.value as! AdvanceOptionalCell
            cell.configCell(data: value)
            cell.AdvanceBtn.addTarget(self, action: #selector(advanceCollapeBtn), for: .touchUpInside)
            return cell
            
        case .repeatCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SetRecurrenceRepeatCell", for: indexPath) as! SetRecurrenceRepeatCell
            let value = data.value as! RepeatCell
            cell.configCell(data: value)
            return cell
        case .inputDescriptionCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputDescriptionCell", for: indexPath) as! InputDescriptionCell
            let value = data.value as! DescriptionCell
            
            cell.configCell(data: value)
            
            return cell
//        case .profileMeetingCell:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "MeetingProfileCell", for: indexPath) as! MeetingProfileCell
//            let value = data.value as! DropdownCell
//            cell.configCell(data: value)
//            return cell
        }
    }
}


extension CreateNewMeetingVC: SelectRoomDelegate, UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {

        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if textView.text == "" {

            textView.text = "Placeholder text ..."
            textView.textColor = UIColor.lightGray
        }
    }
    func selectRoom() {
        let storyboard = UIStoryboard(name: "SelectRoomSB", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SelectRoomVC")
        
        if #available(iOS 16.0, *) {
            if let sheet        = vc.sheetPresentationController {
                let smallId     = UISheetPresentationController.Detent.Identifier("small")
                let smallDetent = UISheetPresentationController.Detent.custom(identifier: smallId) { context in
                    return 380
                }
                sheet.detents = [smallDetent, .large()]
                sheet.largestUndimmedDetentIdentifier = .medium
                sheet.prefersGrabberVisible = true
                }
                present(vc, animated: true, completion: nil)
        }
        
    }
}
